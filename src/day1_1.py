file = open("input.txt", "r")
data = file.read()

example_1 = "(())"
example_2 = "(()(()("
example_3 = ")())())"

test_example_1_up = example_1.count('(')
test_example_1_down = example_1.count(')')

test_example_2_up = example_2.count('(')
test_example_2_down = example_2.count(')')

test_example_3_up = example_3.count('(')
test_example_3_down = example_3.count(')')

def floor(a, b):
    return a - b

def main():
    up = data.count('(')
    down = data.count(')')
    floor = up - down
    print(floor)

if __name__ == '__main__':
    main()